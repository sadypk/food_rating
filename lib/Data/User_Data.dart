import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class CurrentUserData with ChangeNotifier{
  FirebaseUser _user;

  FirebaseUser get user => _user;

  set user(FirebaseUser value) {
    _user = value;
  }

  setUser(FirebaseUser user){
    _user = user;
    notifyListeners();
  }

  logOutUser(){
    _user = null;
  }
}