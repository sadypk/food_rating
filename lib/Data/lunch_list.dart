import 'food.dart';

List<Food> foodList_Lunch = [
  Food(
    foodName: 'Begun',
    foodImage: 'assets/begun.png',
    type: 'Lunch Item'
  ),
  Food(
    foodName: 'Vegetable Curry',
    foodImage: 'assets/vegetable.jpg',
    type: 'Lunch Item'
  ),
  Food(
    foodName: 'Beef',
    foodImage: 'assets/beef.jpg',
    type: 'Lunch Item'
  ),
];