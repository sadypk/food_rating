import 'package:firebase_database/firebase_database.dart';

class Food {
  String foodName;
  String foodImage;
  String type;

  Food({this.foodName, this.foodImage, this.type});


  toJson() {
    return {"foodName":foodName, "foodImage": foodImage, "type": type};
  }

  Food.fromSnapshot(DataSnapshot snapshot){
    key: snapshot.key;
    foodName: snapshot.value["foodName"];
    foodImage: snapshot.value["foodImage"];
    type: snapshot.value["type"];
  }


}
