import 'food.dart';

List<Food> foodList_Snacks = [
  Food(
      foodName: 'Samoosa',
      foodImage: 'assets/samoosa.jpg',
      type: 'Snack Item ',
  ),
  Food(
      foodName: 'Chola',
      foodImage: 'assets/chola.jpg',
      type: 'Snack Item ',
  ),
  Food(
      foodName: 'Butter Bun',
      foodImage: 'assets/butterBun.jpg',
      type: 'Snack Item ',
  ),
  Food(
      foodName: 'Apple',
      foodImage: 'assets/apple.jpg',
      type: 'Snack Item ',
  ),
  Food(
      foodName: 'Grapes',
      foodImage: 'assets/grapes.jpg',
      type: 'Snack Item ',
  ),
];