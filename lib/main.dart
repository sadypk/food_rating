import 'package:flutter/material.dart';
import 'package:foodrating/AdminPanels/Food_Requests.dart';
import 'package:foodrating/AdminPanels/Set_Today_Menu.dart';
import 'package:foodrating/Data/User_Data.dart';
import 'package:foodrating/Login.dart';
import 'package:foodrating/User_Panels/Today_Menu.dart';
import 'package:foodrating/root.dart';
import 'package:provider/provider.dart';
import 'AdminPanels/FoodList/Food_List.dart';
import 'AdminPanels/FoodList/Food_List.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => CurrentUserData(),),
      ],
      child: MaterialApp(
        home: Set_Today_Menu(),
      ),
    );
  }
}
