import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodrating/AdminPanels/Food_Requests.dart';
import 'package:foodrating/root.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';

import 'AdminPanels/FoodList/Food_List.dart';
import 'Data/User_Data.dart';

class User_Drawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Container(
          height: 300,
          child: Center(
            child: Icon(Icons.fastfood, color: Colors.blue,size: 150,),
          ),
        ),
      ],
    );
  }
}


class Admin_Drawer extends StatelessWidget {
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = new GoogleSignIn();
  Future<Null> signOutWithGoogle() async {
    // Sign out with firebase
    await firebaseAuth.signOut();
    // Sign out with google
    await googleSignIn.signOut();
  }
  @override
  Widget build(BuildContext context) {
    var u = Provider.of<CurrentUserData>(context);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        SizedBox(height: 80,),
        Container(
          height: 200,
          margin: EdgeInsets.only(right: 10),
          child: ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: Image.network(u.user.photoUrl, fit: BoxFit.cover,)),
        ),
        SizedBox(height: 20,),
        Text(u.user.displayName, style: TextStyle(fontSize: 20, color: Colors.blue),),
        SizedBox(height: 50,),
        GestureDetector(
          onTap: (){
            Navigator.pop(context);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Food_List()),
            );
          },
          child: Container(
            height: 60,
            width: MediaQuery.of(context).size.width*0.78,
            margin: EdgeInsets.all(5),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black54),
                borderRadius: BorderRadius.circular(5)
            ),
            child: Center(child: Text('Food', style: TextStyle(fontSize: 30, color: Colors.black54, fontWeight: FontWeight.bold),)),
          ),
        ),
        GestureDetector(
          onTap: (){
            Navigator.pop(context);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Food_Requests()),
            );
          },
          child: Container(
            height: 60,
            width: MediaQuery.of(context).size.width*0.78,
            margin: EdgeInsets.all(5),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black54),
                borderRadius: BorderRadius.circular(5)
            ),
            child: Center(child: Text('Today\'s Requests', style: TextStyle(fontSize: 30, color: Colors.black54, fontWeight: FontWeight.bold),)),
          ),
        ),
        GestureDetector(
          onTap: (){
            print('clicked');
            signOutWithGoogle();
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => Root()),
            );
          },
          child: Container(
            height: 60,
            width: MediaQuery.of(context).size.width*0.78,
            margin: EdgeInsets.all(5),
            decoration: BoxDecoration(
                border: Border.all(color: Colors.black54),
                borderRadius: BorderRadius.circular(5)
            ),
            child: Center(child: Text('Logout', style: TextStyle(fontSize: 30, color: Colors.black54, fontWeight: FontWeight.bold),)),
          ),
        ),
      ],
    );
  }
}

