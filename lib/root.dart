import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodrating/AdminPanels/Set_Today_Menu.dart';
import 'package:foodrating/Data/User_Data.dart';
import 'package:provider/provider.dart';

import 'Login.dart';

class Root extends StatefulWidget {
  @override
  _RootState createState() => _RootState();
}

class _RootState extends State<Root> {

  @override
  void initState() {
    // TODO: implement initState
    init();
    super.initState();
  }

  init() async{
    FirebaseUser user = await FirebaseAuth.instance.currentUser().then((u)=>u);
    if(user != null){
      var u =Provider.of<CurrentUserData>(context, listen: false);
      u.setUser(user);
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Set_Today_Menu()),
      );
    }else{
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Login()),
      );
    }
  }


  @override
  Widget build(BuildContext context) {
    return Container(child: CupertinoActivityIndicator(),);
  }
}
