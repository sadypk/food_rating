import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodrating/Data/snacks_list.dart';

import '../Drawer.dart';

class Food_Requests extends StatefulWidget {
  @override
  _Food_RequestsState createState() => _Food_RequestsState();
}

class _Food_RequestsState extends State<Food_Requests> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Snacks Requests',style: TextStyle(color: Colors.black),),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: Container(
        height: height,
        width: width,
        padding: EdgeInsets.only(bottom: 5),
        child: ListView(
          children: <Widget>[
            //Morning Snacks
            SizedBox(height: 20,),
            Container(
              height: height*0.4,
              margin: EdgeInsets.symmetric(horizontal: width*0.02),
              child: Stack(
                children: <Widget>[
                  Container(
                    height: height*0.4,
                    decoration: BoxDecoration(
                        color: Colors.grey[100],
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.blueAccent, width: 2)
                    ),
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: height*0.035,),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: width*0.03),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                height: height*0.16,
                                width: height*0.28,
                                margin: EdgeInsets.only(top: width*0.01),
                                decoration: BoxDecoration(
                                  color: Colors.black54,
                                  borderRadius: BorderRadius.circular(10),
                                  boxShadow: [
                                    BoxShadow(color: Colors.grey[400], offset: Offset(1,1), spreadRadius: 1,blurRadius: 1)
                                  ],
                                  border: Border.all(color: Colors.blueAccent,width: 1)
                                ),
                                child: ClipRRect(
                                  child: Image(image: AssetImage(foodList_Snacks[2].foodImage),fit: BoxFit.cover,),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              Container(
                                height: height*0.14,
                                width: height*0.14,
                                decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    borderRadius: BorderRadius.circular(10),
                                    //border: Border.all(color: Colors.grey[400])
                                    boxShadow: [
                                      BoxShadow(color: Colors.blueAccent, offset: Offset(1,1), spreadRadius: 1,blurRadius: 1)
                                    ]
                                ),
                                child: Center(child: Text('15',style: TextStyle(color: Colors.black,fontSize: 55),),),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: height*0.02,),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: width*0.03),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                height: height*0.16,
                                width: height*0.28,
                                decoration: BoxDecoration(
                                    color: Colors.black54,
                                    borderRadius: BorderRadius.circular(10),
                                    //border: Border.all(color: Colors.grey[400])
                                    boxShadow: [
                                      BoxShadow(color: Colors.grey[400], offset: Offset(1,1), spreadRadius: 1,blurRadius: 1)
                                    ],
                                    border: Border.all(color: Colors.blueAccent,width: 1)
                                ),
                                child: ClipRRect(
                                  child: Image(image: AssetImage(foodList_Snacks[4].foodImage),fit: BoxFit.cover,),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              Container(
                                height: height*0.14,
                                width: height*0.14,
                                decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    borderRadius: BorderRadius.circular(10),
                                    //border: Border.all(color: Colors.grey[400])
                                    boxShadow: [
                                      BoxShadow(color: Colors.blueAccent, offset: Offset(1,1), spreadRadius: 1,blurRadius: 1)
                                    ]
                                ),
                                child: Center(child: Text('8',style: TextStyle(color: Colors.black,fontSize: 55),),),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 20,
                    top: -15,
                    child: Container(
                      height: height*0.06,
                      width: width*0.4,
                      decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.blueAccent, width: 2)
                      ),
                      child: Center(child: Text('Morning Requests', style: TextStyle(color: Colors.blueAccent,fontSize: 15),),),
                    ),
                  )
                ],
                overflow: Overflow.visible,
              ),
            ),

            //Evening Snacks
            SizedBox(height: height*0.05,),
            Container(
              height: height*0.4,
              margin: EdgeInsets.symmetric(horizontal: width*0.02),
              child: Stack(
                children: <Widget>[
                  Container(
                    height: height*0.4,
                    decoration: BoxDecoration(
                        color: Colors.grey[100],
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: Colors.orange, width: 2)
                    ),
                    child: Column(
                      children: <Widget>[
                        SizedBox(height: height*0.035,),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: width*0.03),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                height: height*0.16,
                                width: height*0.28,
                                margin: EdgeInsets.only(top: width*0.01),
                                decoration: BoxDecoration(
                                    color: Colors.black54,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(color: Colors.grey[400], offset: Offset(1,1), spreadRadius: 1,blurRadius: 1)
                                    ],
                                  border: Border.all(color: Colors.orange,width: 2)
                                ),
                                child: ClipRRect(
                                  child: Image(image: AssetImage(foodList_Snacks[1].foodImage),fit: BoxFit.cover,),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              Container(
                                height: height*0.14,
                                width: height*0.14,
                                decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    borderRadius: BorderRadius.circular(10),
                                    //border: Border.all(color: Colors.grey[400])
                                    boxShadow: [
                                      BoxShadow(color: Colors.orange, offset: Offset(1,1), spreadRadius: 1,blurRadius: 1)
                                    ],
                                ),
                                child: Center(child: Text('10',style: TextStyle(color: Colors.black,fontSize: 55),),),
                              )
                            ],
                          ),
                        ),
                        SizedBox(height: height*0.02,),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: width*0.03),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                height: height*0.16,
                                width: height*0.28,
                                decoration: BoxDecoration(
                                    color: Colors.black54,
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(color: Colors.grey[400], offset: Offset(1,1), spreadRadius: 1,blurRadius: 1)
                                    ],
                                    border: Border.all(color: Colors.orange, width: 1)
                                ),
                                child: ClipRRect(
                                  child: Image(image: AssetImage(foodList_Snacks[3].foodImage),fit: BoxFit.cover,),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              Container(
                                height: height*0.14,
                                width: height*0.14,
                                decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    borderRadius: BorderRadius.circular(10),
                                    boxShadow: [
                                      BoxShadow(color: Colors.orange, offset: Offset(1,1), spreadRadius: 1,blurRadius: 1)
                                    ]
                                ),
                                child: Center(child: Text('13',style: TextStyle(color: Colors.black,fontSize: 55),),),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Positioned(
                    left: 20,
                    top: -15,
                    child: Container(
                      height: height*0.06,
                      width: width*0.4,
                      decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(color: Colors.orange, width: 2)
                      ),
                      child: Center(child: Text('Evening Requests', style: TextStyle(color: Colors.orange,fontSize: 15),),),
                    ),
                  )
                ],
                overflow: Overflow.visible,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
