import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:foodrating/Data/User_Data.dart';
import 'package:foodrating/Data/food.dart';
import 'package:foodrating/Data/snacks_list.dart';
import 'package:foodrating/Drawer.dart';
import 'package:foodrating/Authentication.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';

class Set_Today_Menu extends StatefulWidget {
  @override
  _Set_Today_MenuState createState() => _Set_Today_MenuState();
}

class _Set_Today_MenuState extends State<Set_Today_Menu> {
  Food m1,m2,e1,e2;
  int confirmed;
  var now = new DateTime.now();
  @override
  void initState() {
    // TODO: implement initState
    m1=m2=e1=e2=null;
    confirmed =0;
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var u = Provider.of<CurrentUserData>(context);
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Set Today\'s Menu',style: TextStyle(color: Colors.black),),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      drawer: new Drawer(
          child: Admin_Drawer(),
      ),
      body: Container(
        height: height,
        width: width,
        color: Colors.white,
        child: ListView(
          children: <Widget>[
            //Morning Snacks
            Container(
              height: height*0.3,
              width: width-30,
              margin: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Morning Snacks', style: TextStyle(fontSize: height*0.03),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){
                          if(confirmed == 0){
                            popUp(1);
                          }
                        },
                        onDoubleTap: (){
                          if(m1 != null){
                            m1 = null;
                            setState(() {

                            });
                          }
                        },
                        child: Container(
                          height: width*0.4,
                          width: width*0.4,
                          decoration: BoxDecoration(
                              color: Colors.grey[100],
                            borderRadius: BorderRadius.circular(20),
                            border: Border.all(width: 3, color: Colors.black)
                          ),
                          child: m1==null?Center(
                            child: Icon(Icons.add,size: width*0.33,color: Colors.black,),
                          ):ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: Image.asset(m1.foodImage, fit: BoxFit.cover,),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          if(confirmed == 0){
                            popUp(2);
                          }
                        },
                        onDoubleTap: (){
                          if(m2 != null){
                            m2 = null;
                            setState(() {

                            });
                          }
                        },
                        child: Container(
                          height: width*0.4,
                          width: width*0.4,
                          decoration: BoxDecoration(
                              color: Colors.grey[100],
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(width: 3, color: Colors.black54)
                          ),
                          child: m2==null?Center(
                            child: Icon(Icons.add,size: width*0.33,color: Colors.black,),
                          ):ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: Image.asset(m2.foodImage, fit: BoxFit.cover,),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),


            //Evening Snacks
            SizedBox(height: 80,),
            Container(
              height: height*0.3,
              width: width-30,
              margin: EdgeInsets.symmetric(horizontal: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Evening Snacks', style: TextStyle(fontSize: height*0.03),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      GestureDetector(
                        onTap: (){
                          if(confirmed == 0){
                            popUp(3);
                          }
                        },
                        onDoubleTap: (){
                          if(e1 != null){
                            e1 = null;
                            setState(() {

                            });
                          }
                        },
                        child: Container(
                          height: width*0.4,
                          width: width*0.4,
                          decoration: BoxDecoration(
                              color: Colors.grey[100],
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(width: 3, color: Colors.black)
                          ),
                          child: e1==null?Center(
                            child: Icon(Icons.add,size: width*0.33,color: Colors.black,),
                          ):ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: Image.asset(e1.foodImage, fit: BoxFit.cover,),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: (){
                          if(confirmed == 0){
                            popUp(4);
                          }
                        },
                        onDoubleTap: (){
                          if(e2 != null){
                            e2 = null;
                            setState(() {

                            });
                          }
                        },
                        child: Container(
                          height: width*0.4,
                          width: width*0.4,
                          decoration: BoxDecoration(
                              color: Colors.grey[100],
                              borderRadius: BorderRadius.circular(20),
                              border: Border.all(width: 3, color: Colors.black54)
                          ),
                          child: e2==null?Center(
                            child: Icon(Icons.add,size: width*0.33,color: Colors.black,),
                          ):ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: Image.asset(e2.foodImage, fit: BoxFit.cover,),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),

            //Confirm Button
            GestureDetector(
              onTap: (){
                if(confirmed == 0 && m1!=null && m2!=null && e1!=null && e2!=null){
                  confirmed =1;
                  setState(() {

                  });
                };
                print(now.day.toString()+" "+now.month.toString()+" "+now.year.toString());
                Firestore.instance.collection('Menu').document(now.day.toString()+now.month.toString()+now.year.toString()).setData(foodList_Snacks[0].toJson());
              },
              child: Container(
                height: height*0.075,
                margin: EdgeInsets.symmetric(horizontal: 120, vertical: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  color: confirmed==0?Colors.blue:Colors.grey,
                ),
                child: Center(
                  child: Text('Confirm', style: TextStyle(color: Colors.white, fontSize: 25),),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  void popUp(int snackNumber){
    Animation<double> animation;
    AnimationController controller;
    bool dualSelection;
    
    showGeneralDialog(
        context: context,
        barrierDismissible: true,
        barrierLabel: MaterialLocalizations.of(context)
            .modalBarrierDismissLabel,
        barrierColor: Colors.black45,
        transitionDuration: const Duration(milliseconds: 200),
        pageBuilder: (BuildContext buildContext,
            Animation animation,
            Animation secondaryAnimation) {
          var height =MediaQuery.of(context).size.height;
          var width = MediaQuery.of(context).size.width;
          return Stack(
            fit: StackFit.expand,
            children: <Widget>[
              Center(
                child: Container(
                  height: height/1.5,
                  width: width-10,
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Scaffold(
                    body: ListView.builder(
                        itemCount: foodList_Snacks.length,
                        itemBuilder: (BuildContext context, int index){
                          return GestureDetector(
                            onTap: (){
                              if(snackNumber == 1){
                                if(m1 != null){
                                  if(foodList_Snacks[index] == m2){
                                    Scaffold.of(context).showSnackBar(
                                        SnackBar(content: Text('You can not choose the same meals for both morning snacks. Please choose a differant item.'),)
                                    );

                                  }else{
                                    m1 = foodList_Snacks[index];
                                    Navigator.pop(context);
                                  }
                                }else{
                                  if(foodList_Snacks[index] != m2){
                                    m1 = foodList_Snacks[index];
                                    Navigator.pop(context);
                                  }else{
                                    Scaffold.of(context).showSnackBar(
                                        SnackBar(content: Text('You can not choose the same meals for both morning snacks. Please choose a differant item.'),));
                                  }
                                }
                              } else if(snackNumber == 2){
                                if(m2 != null){
                                  if(foodList_Snacks[index] == m1){
                                    Scaffold.of(context).showSnackBar(
                                        SnackBar(content: Text('You can not choose the same meals for both morning snacks. Please choose a differant item.'),)
                                    );
                                  }else{
                                    m2 = foodList_Snacks[index];
                                    Navigator.pop(context);
                                  }
                                }else{
                                  if(foodList_Snacks[index] != m1){
                                    m2 = foodList_Snacks[index];
                                    Navigator.pop(context);
                                  }else{
                                    Scaffold.of(context).showSnackBar(
                                        SnackBar(content: Text('You can not choose the same meals for both morning snacks. Please choose a differant item.'),));
                                  }
                                }
                              } else if(snackNumber == 3){
                                if(e1 != null){
                                  if(foodList_Snacks[index] == e2){
                                    Scaffold.of(context).showSnackBar(
                                        SnackBar(content: Text('You can not choose the same meals for both evening snacks. Please choose a differant item.'),)
                                    );
                                  }else{
                                    e1 = foodList_Snacks[index];
                                    Navigator.pop(context);
                                  }
                                }else{
                                  if(foodList_Snacks[index] != e2){
                                    e1 = foodList_Snacks[index];
                                    Navigator.pop(context);
                                  }else{
                                    Scaffold.of(context).showSnackBar(
                                        SnackBar(content: Text('You can not choose the same meals for both evening snacks. Please choose a differant item.'),));
                                  }
                                }
                              } else if(snackNumber == 4){
                                if(e2 != null){
                                  if(foodList_Snacks[index] == e1){
                                    Scaffold.of(context).showSnackBar(
                                        SnackBar(content: Text('You can not choose the same meals for both evening snacks. Please choose a differant item.'),)
                                    );
                                  }else{
                                    e2 = foodList_Snacks[index];
                                    Navigator.pop(context);
                                  }
                                }else{
                                  if(foodList_Snacks[index] != e1){
                                    e2 = foodList_Snacks[index];
                                    Navigator.pop(context);
                                  }else{
                                    Scaffold.of(context).showSnackBar(
                                        SnackBar(content: Text('You can not choose the same meals for both evening snacks. Please choose a differant item.'),));
                                  }
                                }
                              }
                              setState(() {

                              });
                            },
                            child: Container(
                              height: height*0.1,
                              margin: EdgeInsets.only(bottom: 15),
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.blue[100],
                                  boxShadow: [BoxShadow(color: Colors.blue[200], offset: Offset(1,1),spreadRadius: 1)]
                              ),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    height: height*0.1-10,
                                    width: height*0.1-10,
                                    decoration: BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius: BorderRadius.circular(20),
                                        border: Border.all(color: Colors.blue,width: 1)
                                    ),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(20),
                                      child: Image.asset(foodList_Snacks[index].foodImage, fit: BoxFit.cover,),
                                    ),
                                  ),
                                  Container(
                                    height: height*0.1-10,
                                    width: width*0.65,
                                    child: Center(
                                      child: Text(
                                        foodList_Snacks[index].foodName, style: TextStyle(decoration:TextDecoration.none, color: Colors.black54, fontSize: 20),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          );
                        }),
                  ),
                ),
              ),
              Positioned(
                bottom: 20,
                right: 20,
                child: Container(
                  height: 50,
                  width: 120,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(color: Colors.blue),
                  ),
                  child: Center(
                    child: Text(
                      'Add', style: TextStyle(color: Colors.blue, fontSize: 17),
                    ),
                  ),
                ),
              )
            ],
          );
        });
  }
}
