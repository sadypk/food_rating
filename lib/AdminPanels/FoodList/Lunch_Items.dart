import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:foodrating/Data/lunch_list.dart';
import 'package:foodrating/Data/snacks_list.dart';

class Lunch_Items extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    return Center(
      child: Container(
        height: height-height*0.22,
        width: width*0.95,
        padding: EdgeInsets.symmetric(horizontal: width*0.02,vertical: width*0.02),
        decoration: BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.circular(20),
            boxShadow: [BoxShadow(color: Colors.grey[300], offset: Offset(1,1), spreadRadius: 1)]
        ),
        child: ListView.builder(
            itemCount: foodList_Lunch.length,
            itemBuilder: (BuildContext context, int index){
              return Container(
                height: height*0.2,
                margin: EdgeInsets.only(bottom: 15),
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Colors.blue[100],
                    boxShadow: [BoxShadow(color: Colors.blue[200], offset: Offset(1,1),spreadRadius: 1)]
                ),
                child: Row(
                  children: <Widget>[
                    Container(
                      height: height*0.2-10,
                      width: width*0.5,
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(color: Colors.blue,width: 1)
                      ),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(20),
                        child: Image.asset(foodList_Lunch[index].foodImage, fit: BoxFit.cover,),
                      ),
                    ),
                    SizedBox(width: width*0.02,),
                    Container(
                      width: width*0.35,
                      height: (height*0.2)/2,
                      child: Center(child: Text(foodList_Lunch[index].foodName, style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),)),
                    )
                  ],
                ),
              );
            }),
      ),
    );
  }

  String starRating(int starValue){
    String stars = '';
    for(int i =0 ; i<starValue; i++){
      stars += '⭐ ';
    }
    return stars;
  }
}
