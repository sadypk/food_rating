import 'package:cuberto_bottom_bar/cuberto_bottom_bar.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/fa_icon.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:foodrating/AdminPanels/FoodList/Upload_New_Food.dart';
import '../../Drawer.dart';
import 'Lunch_Items.dart';
import 'Snacks_Items.dart';

class Food_List extends StatefulWidget {
  @override
  _Food_ListState createState() => _Food_ListState();
}

class _Food_ListState extends State<Food_List> {
  Widget w;
  List<Widget> widgetList = [Snacks_Items(),Upload_New_Food()];
  @override
  void initState() {
    // TODO: implement initState
    w = Snacks_Items();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Food List',style: TextStyle(color: Colors.black),),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      drawer: new Drawer(
        child: Admin_Drawer(),
      ),
      bottomNavigationBar: CurvedNavigationBar(
        color: Colors.grey[200],
        backgroundColor: Colors.white,
        buttonBackgroundColor: Colors.blueAccent,
        items: <Widget>[
          Icon(Icons.fastfood),
          Icon(Icons.file_upload),
        ],
        onTap: (index){
          w = widgetList[index];
          setState(() {

          });
        },
        height: height*0.07,
        animationDuration: Duration(milliseconds: 200),
        animationCurve: Curves.bounceInOut,
      ),
      body: w,
    );
  }
}
