import 'dart:io';
import 'package:flutter/material.dart';
import 'package:foodrating/Data/food.dart';
import 'package:foodrating/Data/snacks_list.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Upload_New_Food extends StatefulWidget {
  @override
  _Upload_New_FoodState createState() => _Upload_New_FoodState();
}

class _Upload_New_FoodState extends State<Upload_New_Food> {
  final sort = ['Snack Item', 'Lunch Item'];
  String selectedSort = 'Snack Item';
  File _image;
  final DBref = FirebaseDatabase.instance.reference();
  final nameController = TextEditingController();
  Food food = new Food();

  Future getImage() async{
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
  }

  Future uploadImage(BuildContext context, String fileName) async{
    StorageReference firebaseStorageRef = FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
    var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
    setState(() {
      food.foodImage = dowurl.toString();
      Scaffold.of(context).showSnackBar(SnackBar(content: Text('uploaded'),));
    });
    print('food    '+food.foodImage);
  }

  void uploadDataToDataBase(){
    var data = food.toJson();
    DBref.child("Food_Items_List").push().set(data);
  }


  
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      child: Center(
        child: Container(
          height: height-height*0.22,
          width: width*0.95,
          padding: EdgeInsets.symmetric(horizontal: width*0.03,vertical: width*0.03),
          decoration: BoxDecoration(
              color: Colors.grey[200],
              borderRadius: BorderRadius.circular(20),
              boxShadow: [BoxShadow(color: Colors.grey[300], offset: Offset(1,1), spreadRadius: 1)]
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Upload a new item', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
              SizedBox(height: 40,),

              //Item Name TextFormField
              Container(
                height: 45,
                width: width-width*0.1,
                padding: EdgeInsets.only(left: 10),
                decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: [BoxShadow(color: Colors.grey[400], offset: Offset(1,1), spreadRadius: 1)]
                ),
                child: TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(
                    hintText: 'Item Name',
                    icon: Icon(Icons.edit),
                  ),
                ),
              ),

              //Item Type DropDownMenu
              SizedBox(height: 30,),
              Container(
                height: 60,
                width: 185,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: [BoxShadow(color: Colors.grey[400], offset: Offset(1,1), spreadRadius: 1)],
                    color: Colors.grey[300]
                ),
                child: Padding(
                  padding: EdgeInsets.only(left: 15,top: 5,right: 15),
                  child: DropdownButtonFormField<String>(
                    decoration: InputDecoration.collapsed(hintText: null),
                    items: sort.map((String value){
                      return DropdownMenuItem(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
                    value: selectedSort,
                    onChanged: (String value){
                      setState(() {
                        this.selectedSort = value;
                      });
                    },
                  ),
                ),
              ),

              //Image Preview
              SizedBox(height: 30,),
              Container(
                height: width*0.3,
                width: width*0.3,
                decoration: BoxDecoration(
                    color: Colors.grey[100],
                    borderRadius: BorderRadius.circular(20),
                    border: Border.all(width: 3, color: Colors.grey[400])
                ),
                child: _image==null?Center(
                  child: Icon(Icons.add,size: width*0.15,color: Colors.grey[400],),
                ):ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Image.file(_image, fit: BoxFit.cover,)),
              ),
              SizedBox(height: 20,),
              //Upload Item Image Button
              GestureDetector(
                onTap: (){
                  print('clicked');
                  getImage();
                },
                child: Container(
                  height: 40,
                  width: 140,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      boxShadow: [BoxShadow(color: Colors.grey[400], offset: Offset(1,1), spreadRadius: 1)],
                      color: Colors.grey[300]
                  ),
                  child: Center(child: Row(
                    children: <Widget>[
                      SizedBox(width: 10,),
                      Icon(Icons.file_upload, color: Colors.grey[500],),
                      SizedBox(width: 10,),
                      Text('Upload Image', style: TextStyle(color: Colors.black),)
                    ],
                  ),)
                ),
              ),
              //Upload item button
              SizedBox(height: 40,),
              Align(
                alignment: Alignment.center,
                child: GestureDetector(
                  onTap: (){
                    print('clicked');
                    uploadImage(context, nameController.text);
                    setState(() {
                      food.foodName = nameController.text;
                      food.type = selectedSort;
                    });
                    Firestore.instance.collection('Food_Items_List').document().setData(food.toJson());
                  },
                  child: Container(
                    height: 50,
                    width: 150,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: Colors.blueAccent
                    ),
                    child: Center(
                      child: Text('Upload Item', style: TextStyle(color: Colors.white, fontSize: 17),),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  onDropDownChanged(String value){
    setState(() {
      this.selectedSort = value;
    });
  }
}
