import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:foodrating/Data/food.dart';
import 'package:foodrating/Data/snacks_list.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Snacks_Items extends StatefulWidget {
  @override
  _Snacks_ItemsState createState() => _Snacks_ItemsState();
}

class _Snacks_ItemsState extends State<Snacks_Items> {

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Center(
      child: Container(
        height: height-height*0.22,
        width: width*0.95,
        padding: EdgeInsets.symmetric(horizontal: width*0.02,vertical: width*0.02),
        decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.circular(20),
          boxShadow: [BoxShadow(color: Colors.grey[300], offset: Offset(1,1), spreadRadius: 1)]
        ),
        child: BookList(),
      ),
    );
  }

  var snacksList = Container(
    color: Colors.red,
  );

  String starRating(int starValue){
    String stars = '';
    for(int i =0 ; i<starValue; i++){
      stars += '⭐ ';
    }
    return stars;
  }
}

class BookList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('Food_Items_List').snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError)
          return new Text('Error: ${snapshot.error}');
        switch (snapshot.connectionState) {
          case ConnectionState.waiting: return new Text('Loading...');
          default:
            return new ListView(
              children: snapshot.data.documents.map((DocumentSnapshot document) {
                return new ListTile(
                  title: new Text(document['foodName']),
                  subtitle: new Text(document['type']),
                );
              }).toList(),
            );
        }
      },
    );
  }
}