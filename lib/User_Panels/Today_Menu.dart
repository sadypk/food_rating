import 'package:flutter/material.dart';
import 'package:foodrating/User_Panels/Selected_Items.dart';
import 'package:foodrating/Data/snacks_list.dart';
import '../Data/food.dart';
import '../Drawer.dart';

class Today_Menu extends StatefulWidget {
  @override
  _Today_MenuState createState() => _Today_MenuState();
}

class _Today_MenuState extends State<Today_Menu> {
  int mi, ei, submitted;
  Food selcted_morning_snack, selected_evening_snack;

  @override
  void initState() {
    // TODO: implement initState
    mi = ei = 1;
    submitted = 0;
    selcted_morning_snack = foodList_Snacks[0];
    selected_evening_snack = foodList_Snacks[2];
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('Today\'s Menu',style: TextStyle(color: Colors.black),),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      drawer: new Drawer(
        child: User_Drawer(),
      ),
      body: Container(
        height: height,
        width: width,
        color: Colors.white,
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                //Morning Snacks
                Container(
                  height: height/2.5,
                  width: width-30,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Morning Snacks', style: TextStyle(fontSize: height*0.03),),
                      Container(
                        height: height/3,
                        width: width-30,
                        margin: EdgeInsets.only(top: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            MorningSnacks(1, foodList_Snacks[0]),
                            MorningSnacks(2, foodList_Snacks[1]),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),

                SizedBox(height: height*0.01,),
                //EveningSnacks
                Container(
                  height: height/2.5,
                  width: width-30,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text('Evening Snacks', style: TextStyle(fontSize: height*0.03),),
                      Container(
                        height: height/3,
                        width: width-30,
                        margin: EdgeInsets.only(top: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            EveningSnacks(1, foodList_Snacks[2]),
                            EveningSnacks(2, foodList_Snacks[3]),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: (){
                    submitted = 1;
                    setState(() {

                    });
                  },
                  child: Container(
                    margin: EdgeInsets.only(top: height*0.005),
                    height: height*0.05,
                    width: width*0.4,
                    decoration: BoxDecoration(
                        color: submitted == 0? Colors.blueAccent:Colors.grey,
                        borderRadius: BorderRadius.circular(5)
                    ),
                    child: Center(
                      child: Text(submitted == 0?'Submit Choice':'Submitted', style: TextStyle(fontSize: height*0.025,color: Colors.white),),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget MorningSnacks(int index, Food food){
    var height = (MediaQuery.of(context).size.height)/3;
    return GestureDetector(
      onTap: (){
        if(submitted == 0){
          mi = index;
        }
        selcted_morning_snack = food;
        setState(() {

        });
      },
      child: Opacity(
        opacity: index == mi?1: .5,
        child: Container(
          height: height,
          width: 180,
          child: Stack(
            children: <Widget>[
              Positioned(
                bottom: 0,
                child: Container(
                  height: height/4,
                  width: 180,
                  padding: EdgeInsets.only(top: 25),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: index == mi?Colors.blueAccent:Colors.grey[100],
                    boxShadow: [
                      BoxShadow(
                        color: index == ei?Colors.grey:Colors.black54,
                        offset: Offset(1,1),
                        spreadRadius: 1,
                        blurRadius: 1
                      )
                    ]
                  ),
                  child: Center(
                    child: Text(food.foodName, style: TextStyle(color: index == mi?Colors.white:Colors.black54, fontSize: 20),),
                  ),
                ),
              ),
              Positioned(
                top: 0,
                child: Container(
                  height: height*0.85,
                  width: 160,
                  margin: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                    boxShadow: [
                      BoxShadow(
                        offset: Offset(1,1),
                        color: Colors.black45,
                        spreadRadius: 1
                      )
                    ]
                  ),
                  child: ClipRRect(
                    child: Hero(
                      tag: index==mi?'morning selected':'morning notSelected',
                      child: Image(
                        image: AssetImage(food.foodImage),
                        fit: BoxFit.cover,
                      ),
                    ),
                    borderRadius: BorderRadius.circular(15),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget EveningSnacks(int index, Food food){
    var height = (MediaQuery.of(context).size.height)/3;
    return GestureDetector(
      onTap: (){
        if(submitted == 0){
          ei = index;
        }
        selected_evening_snack = food;
        setState(() {

        });
      },
      child: Opacity(
        opacity: index == ei?1: .5,
        child: Container(
          height: height,
          width: 180,
          child: Stack(
            children: <Widget>[
              Positioned(
                bottom: 0,
                child: Container(
                  height: height/4,
                  width: 180,
                  padding: EdgeInsets.only(top: 25),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: index == ei?Colors.blueAccent:Colors.grey[100],
                      boxShadow: [
                        BoxShadow(
                            color: index == ei?Colors.grey:Colors.black54,
                            offset: Offset(1,1),
                            spreadRadius: 1,
                            blurRadius: 1
                        )
                      ]
                  ),
                  child: Center(
                    child: Text(food.foodName, style: TextStyle(color: index == ei?Colors.white:Colors.black54, fontSize: 20),),
                  ),
                ),
              ),
              Positioned(
                top: 0,
                child: Container(
                  height: height*0.85,
                  width: 160,
                  margin: EdgeInsets.only(left: 10, right: 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: [
                        BoxShadow(
                            offset: Offset(1,1),
                            color: Colors.black45,
                            spreadRadius: 1
                        )
                      ]
                  ),
                  child: ClipRRect(
                    child: Hero(
                      tag: index==ei?'evening selected':'evening notSelected',
                      child: Image(
                        image: AssetImage(food.foodImage),
                        fit: BoxFit.cover,
                      ),
                    ),
                    borderRadius: BorderRadius.circular(15),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
