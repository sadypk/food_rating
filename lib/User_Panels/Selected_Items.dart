import 'dart:io';

import 'package:flutter/material.dart';
import 'package:foodrating/Drawer.dart';

import '../Data/food.dart';

class Selected_Items extends StatefulWidget {
  final Food morning, evening;
  Selected_Items({Key key, @required this.morning,this.evening}) : super(key: key);
  @override
  _Selected_ItemsState createState() => _Selected_ItemsState();
}

class _Selected_ItemsState extends State<Selected_Items> {
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return new WillPopScope(
      onWillPop: (){
        exit(0);
      },
      child: Scaffold(
        appBar: AppBar(
          title: Text('Today\'s Menu',style: TextStyle(color: Colors.black),),
          centerTitle: true,
//          leading: Icon(Icons.menu, size: height*0.04,color: Colors.black,),
          elevation: 0,
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Colors.black),
        ),
        drawer: new Drawer(
          child: User_Drawer(),
        ),
        body: Container(
          height: height,
          width: width,
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 15),
                child: Text('Snacks:', style: TextStyle(fontSize: height*0.03),),
              ),
              Container(
                  height: (height/3)*0.85,
                  width: width-30,
                  margin: EdgeInsets.only(left: 15, right: 15),
                  child: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      Container(
                        height: height/4,
                        width: width-30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [BoxShadow(color: Colors.grey[500], spreadRadius: 1, offset: Offset(1,1))]
                        ),
                        child: ClipRRect(child: Hero(tag: 'morning selected',child: Image.asset(widget.morning.foodImage,fit: BoxFit.cover,)),borderRadius: BorderRadius.circular(10),),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          height: height*0.065,
                          width: width*0.35,
                          padding: EdgeInsets.only(left: 5),
                          decoration: BoxDecoration(
                              color: Colors.white54,
                              borderRadius: BorderRadius.circular(5)
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text('Morning',style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold),),
                              Text(widget.morning.foodName,style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold,fontSize: 24),)
                            ],
                          ),
                        ),
                      ),
//                      Positioned(
//                        right: 5,
//                        bottom: 5,
//                        child: Container(
//                          height: height*0.04,
//                          width: width*0.35,
//                          decoration: BoxDecoration(
//                              color: Colors.black26,
//                              borderRadius: BorderRadius.circular(5)
//                          ),
//                          child: Center(child: Text('⭐ ⭐ ⭐ ⭐', style: TextStyle(color: Colors.yellow, fontSize: 18),)),
//                        ),
//                      )
                    ],
                  )
              ),
              SizedBox(height: 25,),
              Container(
                  height: height/4,
                  width: width-30,
                  margin: EdgeInsets.only(left: 15, right: 15),
                  child: Stack(
                    fit: StackFit.expand,
                    children: <Widget>[
                      Container(
                        height: height/4,
                        width: width-30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: [BoxShadow(color: Colors.grey[500], spreadRadius: 1, offset: Offset(1,1))]
                        ),
                        child: ClipRRect(child: Hero(tag: 'evening selected',child: Image.asset(widget.evening.foodImage,fit: BoxFit.cover,)),borderRadius: BorderRadius.circular(10),),
                      ),
                      Positioned(
                        bottom: 5,
                        left: 5,
                        child: Container(
                          height: height*0.065,
                          width: width*0.35,
                          padding: EdgeInsets.only(left: 5),
                          decoration: BoxDecoration(
                              color: Colors.white54,
                              borderRadius: BorderRadius.circular(5)
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text('Evening',style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold),),
                              Text(widget.evening.foodName,style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold,fontSize: 24),)
                            ],
                          ),
                        ),
                      ),
//                      Positioned(
//                        right: 5,
//                        bottom: 5,
//                        child: Container(
//                          height: height*0.04,
//                          width: width*0.35,
//                          decoration: BoxDecoration(
//                              color: Colors.black26,
//                              borderRadius: BorderRadius.circular(5)
//                          ),
//                          child: Center(child: Text('⭐ ⭐ ⭐ ⭐ ⭐', style: TextStyle(color: Colors.yellowAccent, fontSize: 18),)),
//                        ),
//                      )
                    ],
                  )
              ),

              SizedBox(height: 30,)
            ],
          )
        ),
      ),
    );
  }


}
