import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:foodrating/AdminPanels/Set_Today_Menu.dart';
import 'package:foodrating/User_Panels/Today_Menu.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:foodrating/root.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:foodrating/Authentication.dart';


class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width =MediaQuery.of(context).size.width;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: height,
          width: width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Container(
                height: height*0.4,
                width: width*0.8,
                alignment: Alignment.center,
                child: FlareActor("assets/Food.flr",fit: BoxFit.cover, animation: 'loading',),
              ),
              SizedBox(height: 50),
              GestureDetector(
                onTap: (){
                  handleSignIn()
                      .then((FirebaseUser user) => print(user))
                      .catchError((e) => print(e));
                },
                child: Container(
                    height:100,
                    width:100,
                    child: ClipRRect(child: Image.asset('assets/google.png'),borderRadius: BorderRadius.circular(40),)),
              ),
              SizedBox(height: 50),
              GestureDetector(
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Root()),
                  );
                },
                child: Container(
                  height: height*0.06,
                  width: width*0.4,
                  decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(40),
                  ),
                  child: Center(
                    child: Text(
                      'Login',style: TextStyle(color: Colors.white, fontSize: 17),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      )
    );
  }
}
